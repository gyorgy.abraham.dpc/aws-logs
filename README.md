# awstail
A simple way to see your Amazon CloudWatch logs

## Install

Simply clone this repository and run `./gradlew build`. The CLI tool will be in the `/build/libs/` library as `./awstail`

## Features

* Easy to use CLI
* Fetch or tail a log file (in realtime) - lists every event on every stream in a log group
* List log file names
* Filter a log file between a start and end time using natural language (e.g. **"3 minutes ago"**)
* Set common prefixes and/or suffixes as environment variables or arguments, so you only have to define the changing parts (e.g.  prefix -> **"/aws/elasticbeanstalk/" + logToFind + "/var/log/web-1.error.log"** <- suffix)
* Use with other CLI tools via pipe (e.g. `./awstail -g | grep dev`)

## Arguments, environment variables

| Argument             | Explenation      |
|----------------------|------------------|
| -f, --follow         | Tail the log     |
| -g, --groups         | Write the groups |
| --since, --from      | Start time       |
| --to                 | End time         |
| -p, --prefix         | Log prefix       |
| -s, --suffix         | Log suffix       |
| -h, --help           | Show help        |

To set up default Logfile prefix and suffix use environment variables: 
 * AWSTAIL_PREFIX
 * AWSTAIL_INFIX
 * AWSTAIL_SUFFIX
 
## Examples

* `./awstail -g` - list groups
* `./awstail dev` (with prefix and suffix set in environment variables) - fetches the logs from prefix + "dev" + suffix logged in the last 10 minutes (10 minutes is the default)
* `./awstail dev -f` - tail logs from prefix + "dev" + suffix
* `./awstail dev -p /aws/elasticbeanstalk/ -s /var/log/web-1.error.log` - define prefix and suffix (overrides environment variables) and fetches the following log file: `/aws/elasticbeanstalk/dev/var/log/web-1.error.log`
* `./awstail dev --since "3 minutes ago"` - fetches everything logged in the last 3 minutes
* `./awstail dev --from "10-10-1992 15:40" --to "10-10-1992 15:45"` - fetches log events between specific dates

### See also

[Natty](http://natty.joestelmach.com/)