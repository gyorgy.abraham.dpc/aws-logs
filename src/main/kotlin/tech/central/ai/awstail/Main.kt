package tech.central.ai.awstail

import com.beust.jcommander.JCommander
import tech.central.ai.awstail.writers.ConsoleGroupWriter
import tech.central.ai.awstail.writers.ConsoleLogWriter


/**
 * AWSTail - Tail logs from Amazon CloudWatch with follow option
 */
fun main(args: Array<String>) {
    val arguments = AWSTailArguments()
    val jCommander = parseArgs(arguments, args)
    val awsLogs = AWSLogsClient(arguments)

    if (arguments.groups) {
        ConsoleGroupWriter(awsLogs).write()
    } else if (arguments.version) {
        writeVersion()
    } else if (arguments.help || arguments.infix.isEmpty()) {
        writeHelp(jCommander)
    } else {
        ConsoleLogWriter(awsLogs).write()
    }
}

fun writeVersion() {
 println("AWS Tail v1.5")
}

private fun parseArgs(logs: AWSTailArguments, args: Array<String>): JCommander {
    val jCommander = JCommander.newBuilder()
            .addObject(logs)
            .build()
    jCommander.parse(*args)
    return jCommander
}

private fun writeHelp(jCommander: JCommander) {
    println("""AWSTail - Tail logs from Amazon CloudWatch with follow option

Displays every log from every stream based in a logGroup based on its name
 * logGroupName is as follows: prefix + infix + suffix
 * where prefix and suffix are optional
""")

    jCommander.usage()

    println("""
Or pass default parameters from environment variables:
 * AWSTAIL_PREFIX
 * AWSTAIL_INFIX
 * AWSTAIL_SUFFIX
""")
}
