package tech.central.ai.awstail

import com.beust.jcommander.Parameter
import tech.central.ai.awstail.utils.DateConverter
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * Resolve command line arguments and holds information for writing the log
 *
 * logGroupName is as follows: prefix + infix + suffix
 * where prefix and suffix are optional
 *
 */
class AWSTailArguments(
        @Parameter(names = ["-f", "--follow"], description = "Whether to follow the log or not")
        var follow: Boolean = false,

        @Parameter(description = "LogGroup name infix")
        var infix: String = "",

        @Parameter(names = ["-p", "--prefix"], arity = 1, description = "LogGroup name prefix")
        var prefix: String? = null,

        @Parameter(names = ["-s", "--suffix"], arity = 1, description = "LogGroup name suffix")
        var suffix: String? = null,

        @Parameter(names = ["--since", "--from", "--start"], arity = 1, description = "Start time to log from", converter = DateConverter::class)
        var startTime: Long? = null,

        @Parameter(names = ["--to", "--until", "--end"], arity = 1, description = "End time to log to", converter = DateConverter::class)
        var endTime: Long? = null,

        @Parameter(names = ["-n"], description = "The number of lines to download at once")
        var n: Int = 100,

        @Parameter(names = ["-g", "--groups"], description = "Show groups only")
        var groups: Boolean = false,

        @Parameter(names = ["--help", "-h"], help = true)
        var help: Boolean = false,

        @Parameter(names = ["--version", "-v"], help = true)
        var version: Boolean = false
) {
    val logGroupName
        get() = prefix.orEmpty() + infix + suffix.orEmpty()

    init {
        startTime = startTime ?: Instant.now().minus(10, ChronoUnit.MINUTES).toEpochMilli()
    }
}