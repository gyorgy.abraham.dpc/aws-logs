package tech.central.ai.awstail.writers

import com.amazonaws.services.logs.model.FilteredLogEvent
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.awstail.utils.TerminalColor
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime

class ConsoleLogWriter(awsLogs: AWSLogsClient) : LogWriter(awsLogs) {


    companion object {
        val Palette = arrayOf(TerminalColor.COLOR_GREEN, TerminalColor.COLOR_YELLOW, TerminalColor.COLOR_BLUE, TerminalColor.COLOR_PURPLE, TerminalColor.COLOR_LBLUE).iterator()
        val colors: MutableMap<String, TerminalColor> = HashMap()
    }

    override fun tailLogs() {
        do {
            fetchLogs()
            Thread.sleep(1000)
        } while (true)
    }

    override fun writeFailMessage(failMessage: String) = println(failMessage)

    override fun writeLogsIntroduction() {
        val verb = if (awsLogs.arguments.follow) "Tailing" else "Fetching"
        println("$verb logs for ${awsLogs.arguments.logGroupName}..." colored TerminalColor.COLOR_GREEN)
        awsLogs.arguments.startTime?.let {
            println("Since: ${prettyPrintTimestamp(it)}" colored TerminalColor.COLOR_GREEN)
        }
        awsLogs.arguments.endTime?.let {
            println("Until: ${prettyPrintTimestamp(it)}" colored TerminalColor.COLOR_GREEN)
        }
    }

    override fun writeEvent(event: FilteredLogEvent) =
            println("${event.logStreamName.substring(0, 8) colored streamColor(event.logStreamName)}:  ${event.message}")

    private infix fun String.colored(color: TerminalColor): String = "${color.escapeSequence}$this${TerminalColor.COLOR_RESET.escapeSequence}"

    private fun streamColor(logStreamName: String): TerminalColor {
        if (!colors.containsKey(logStreamName) && Palette.hasNext()) {
            colors[logStreamName] = Palette.next()
        }

        return colors[logStreamName] ?: TerminalColor.COLOR_RED
    }

    private fun prettyPrintTimestamp(epochmillis: Long): ZonedDateTime =
            ZonedDateTime.of(
                    LocalDateTime.ofEpochSecond(epochmillis / 1000, 0, ZoneOffset.UTC),
                    ZoneId.of("UTC+00:00")
            )

}