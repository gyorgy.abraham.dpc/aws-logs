package tech.central.ai.awstail.utils

import com.amazonaws.regions.AwsRegionProvider

class AwsStaticRegionProvider(val regionInput: String?) : AwsRegionProvider() {
    override fun getRegion(): String? = regionInput
}