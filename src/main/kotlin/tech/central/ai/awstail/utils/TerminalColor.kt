package tech.central.ai.awstail.utils


enum class TerminalColor(val escapeSequence: CharSequence) {
    COLOR_RED("\u001B[31m"),
    COLOR_GREEN("\u001B[32m"),
    COLOR_YELLOW("\u001B[33m"),
    COLOR_BLUE("\u001B[34m"),
    COLOR_PURPLE("\u001B[35m"),
    COLOR_LBLUE("\u001B[36m"),
    COLOR_RESET("\u001B[0m")
}