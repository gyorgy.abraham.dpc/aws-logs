package tech.central.ai.awstail.utils

import com.beust.jcommander.IStringConverter
import com.joestelmach.natty.Parser
import java.time.Instant


class DateConverter : IStringConverter<Long> {
    override fun convert(value: String?): Long =
            (Parser()
                    .parse(value + " UTC")
                    .toList()
                    .firstOrNull()
                    ?.dates
                    ?.toList()
                    ?.firstOrNull()
                    ?.toInstant() ?: Instant.now()).toEpochMilli()
}